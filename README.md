# bdd-cucumber-framework
BDD Cucumber Framework with Gradle
 
 Framework is built using karate DSL, I choose to use karate framework instead of Serenity BDD, since in karate we doesn’t need to write any step-definition. Which saves our lot of time to build test cases and makes test case writing easier , Karate is built on top of cucumber framework and with inbuilt step level detailed reporting. 

Please find the list of tools used to build the framework
![](img/FrameworkDetail.PNG)

**FrameWork structure**

   - TestRunner is the java class from where execution starts
   - Karate-config.js is the file where all the global variable are set, different set of global variables will be picked based on the env details passed. like staging, prodection etc
   - logback-test.xml is used to print logs in console
   - features folder contains all the test cases
   - requestJons folder contains all the required input jsons
   - reusable folder contains, pre-req data creation methods like authentication, which can be reused acroos     all feature files.
   - target folder contains karate and cucumber reports

![](img/FrameworkStructure.PNG)

**Test case Details**

     I have picked hotel booking application to automate, so that i can cover all different http requests, along with authentication, negative cases and grouping of test cases.
     - Feature file structure
       - background: setting all the global variable  like URLs, headers.
       - username and password are externalized and picked from karate-config.js.
       - username, password is passed to Authentication method of reusable feature file which returns token. later token is set to global variable
     - Grouping of feature file is done by using tags. like @createbooking @updateBooking , which helps us to execute only required group of test cases.
     - CreateBooking feature file contains post request to create new booking and validating the booking by performing get request with same id.
     - UpdateBooking feature contains put requests which valid/invalid ids and trying to update without cookie header  
     - DeleteBooking feature contains delete Requests with valid and invalid ids
     - PatchBooking feature contains valid patch request and trying to patch the booking with put request, which is negative scenario.

**How to write test cases ?**

     - Create a new feature file under features folder
     - Under Background keyword set all the global variables and prerequired data
     - Each Scenario / scenario outline will be considered as one test case. we use scenario outline when we have to add validation under Example tag in the test cases
     - Externalize all the global variables, environment specific variables in Karate-confog.js
     - Test case is written in Grekin language with no step-definition required. Just follow Given, When, And, Then
     - Tag each test cases to group, like : @Booking

**How to execute test cases ?**
     
     - Default all test cases gets execute in 4 threads
     - Execute from IDE - right click on Testrunner.java , Run as Junit Test
     - Execute from command prompt 
         - Group by feature file and stage env details
          command :- gradle test "-Dkarate.options=src/test/resources/features/Test.feature"  -Dtest=TestRunner -Denv=stage --info
         - Group by tags and prod env details
          command :- gradle test "-Dkarate.options=--tags @createBooking"  -Dtest=TestRunner -Denv=prod
     - GitLab CI all cases gets executed by default after each commit

**Cucumber html report in target folder**

![](img/CucumberReport.PNG)

**Karate detailed report in target folder**

![](img/KarateReporting.PNG)

**Console logs During excution**

![](img/ConsoleLog.PNG)

**GitLab Ci**
   
   Gitlab runner is enabled and valid YAML file is written to execute all test cases on each commit. But even with gitlab free verion, we need to give credit card details to execute CI in gitlab, else getting below error. so i dint take risk of giving my card details.
![](img/ValidateAcoountForCI.PNG)

