@booking 
Feature: Booking API Testing

  Background: 
    * url baseUrl
    * header Content-Type = 'application/json'
    * header Accept = 'application/json'
    * json login = { "username" : '#(adminUname)' , "password" : '#(adminPwd)' }
   
    * print 'login................',login

  @authentication
  Scenario: Authentice the user
    Given url 'https://restful-booker.herokuapp.com/auth'
    And request '{ "username" : "admin","password" : "password123" }'
    When method post
    Then status 200
    Then print 'response.token................',response.token
    

    