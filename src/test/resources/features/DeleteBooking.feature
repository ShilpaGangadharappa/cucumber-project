@booking @parallel=false @reporter_shilpaDG
Feature: Booking API Testing

  Background: 
    #get base url value from config file
    * url baseUrl
    
    #set headers as global parameters
    * header Content-Type = 'application/json'
    
    #get authentication token
    * def authentication = callonce read('classpath:reusable/reusable.feature@authentication') {"username" : '#(adminUname)',"password" : '#(adminPwd)'}
    * string authToken = authentication.response.token
    * print 'authToken................',authToken
    * string tokenstr = 'token=' + authToken
    
    #json file paths
    * json createBooking = read('classpath:requestJsons/createBooking.json')
    
  
	@booking @deleteBooking
  Scenario: Delete booking and validate values
     #Create new booking
    Given url baseUrl + '/booking'
    Given header Content-Type = 'application/json'
    Given header Accept = 'application/json'
    Given header Authorization = authToken
    And request createBooking
    When method post
    Then status 200
    Then print 'new booking id created................',response.bookingid
    
    #Delete booking
  Given string tokenstr = 'token=' + authToken
    Given url 'https://restful-booker.herokuapp.com/booking/' + response.bookingid
    Given header Content-Type = 'application/json'
    Given header Cookie = tokenstr
    When method DELETE
    Then status 201
    Then print 'Delete................',response
    
    
    @booking @deleteBooking
  Scenario: Delete booking with invalid id
     
    #Delete booking
  Given string tokenstr = 'token=' + authToken
    Given url 'https://restful-booker.herokuapp.com/booking/200'
    Given header Content-Type = 'application/json'
    Given header Cookie = tokenstr
    When method DELETE
    Then status 405
    Then print 'Delete with invalid id................',response
   
	
	
	