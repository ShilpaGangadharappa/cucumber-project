@booking @parallel=false @reporter_shilpaDG
Feature: Booking API Testing

  Background: 
    #get base url value from config file
    * url baseUrl
    
    #set headers as global parameters
    * header Content-Type = 'application/json'
    
    #get authentication token
    * def authentication = callonce read('classpath:reusable/reusable.feature@authentication') {"username" : '#(adminUname)',"password" : '#(adminPwd)'}
    * string authToken = authentication.response.token
    * print 'authToken................',authToken
    * string tokenstr = 'token=' + authToken
    
    #json file paths
    * json patchBooking = read('classpath:requestJsons/patchBooking.json')
    	
  @booking @patchBooking
  Scenario Outline: Patch booking and validate values

     #Patch booking
  Given string tokenstr = 'token=' + authToken
    Given url 'https://restful-booker.herokuapp.com/booking/4'
    Given header Content-Type = 'application/json'
    Given header Accept = 'application/json'
    #Given header Authorization = authToken
    Given header Cookie = tokenstr
    And request patchBooking
    When method patch
    Then status 200
     And match <Firstname>
    And match <Lastname>
    Then print 'Patch................',response.bookingid
    
    Examples:
	|Firstname |Lastname |
	|response.firstname == 'JamesPatch'|response.lastname == 'Brown'|
	
	 @booking @patchBooking
  Scenario: Patch booking with invalid method name

     #Patch booking
  Given string tokenstr = 'token=' + authToken
    Given url 'https://restful-booker.herokuapp.com/booking/1'
    Given header Content-Type = 'application/json'
    Given header Accept = 'application/json'
    Given header Cookie = tokenstr
    And request patchBooking
    When method put
    Then status 400
    Then print 'Patch................',response
	