@booking @parallel=false @reporter_shilpaDG
Feature: Booking API Testing

  Background: 
    #get base url value from config file
    * url baseUrl
    
    #set headers as global parameters
    * header Content-Type = 'application/json'
    
    #get authentication token
    * def authentication = callonce read('classpath:reusable/reusable.feature@authentication') {"username" : '#(adminUname)',"password" : '#(adminPwd)'}
    * string authToken = authentication.response.token
    * print 'authToken................',authToken
    * string tokenstr = 'token=' + authToken
    
    #json file paths
    * json createBooking = read('classpath:requestJsons/createBooking.json')
   
   @booking @createBooking
  Scenario Outline: Create booking and validate by get booking api
  #Create new booking
    Given url baseUrl + '/booking'
    Given header Content-Type = 'application/json'
    Given header Accept = 'application/json'
    Given header Authorization = authToken
    And request createBooking
    When method post
    Then status 200
    Then print 'new booking id created................',response.bookingid
    
  #Validate booking by get booking details
    Given url baseUrl + '/booking/' + response.bookingid
    Given header Accept = 'application/json'
    Given header Authorization = authToken
    When method get
    Then status 200
    And match <Firstname>
    And match <Lastname>
    Then print 'booking details of new booking................',response
    Examples:
	|Firstname |Lastname |
	|response.firstname == 'Jim'|response.lastname == 'Brown'|
	
		