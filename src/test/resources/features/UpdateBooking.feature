@booking @parallel=false @reporter_shilpaDG
Feature: Booking API Testing

  Background: 
    #get base url value from config file
    * url baseUrl
    
    #set headers as global parameters
    * header Content-Type = 'application/json'
    
    #get authentication token
    * def authentication = callonce read('classpath:reusable/reusable.feature@authentication') {"username" : '#(adminUname)',"password" : '#(adminPwd)'}
    * string authToken = authentication.response.token
    * print 'authToken................',authToken
    * string tokenstr = 'token=' + authToken
    
    #json file paths
    * json updateBooking = read('classpath:requestJsons/updateBooking.json')
 
	@booking @updateBooking
  Scenario Outline: Update booking and validate values
 
  #Update new booking
    Given url baseUrl + '/booking/3'
    Given header Content-Type = 'application/json'
    Given header Accept = 'application/json'
    #Given header Authorization = authToken
    Given header Cookie = tokenstr
    And request updateBooking
    When method put
    Then status 200
     And match <Firstname>
    And match <Lastname>
    Then print 'Update booking................',response
    
    Examples:
	|Firstname |Lastname |
	|response.firstname == 'JamesUpdate'|response.lastname == 'Brown'|
	
	@booking @updateBooking
  Scenario: Update booking with invalid cookie token
 
  #Update new booking
    Given url 'https://restful-booker.herokuapp.com/booking/1'
    Given header Content-Type = 'application/json'
    Given header Accept = 'application/json'
    Given header Cookie = authToken
    And request updateBooking
    When method put
    Then status 403
    Then print 'Error message for invalid cookie................',response
    
    	@booking @updateBooking
  Scenario: Update booking with invalid id
 
  #Update new booking
    Given url 'https://restful-booker.herokuapp.com/booking/111'
    Given header Content-Type = 'application/json'
    Given header Accept = 'application/json'
    Given header Cookie = tokenstr
    And request updateBooking
    When method put
    Then status 405
    Then print 'Error message for invalid cookie................',response
    