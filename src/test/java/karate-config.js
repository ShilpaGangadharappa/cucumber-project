function fn() {
	var env = karate.env; // get java system property 'karate.env'
    karate.log('karate.env selected environment was:', env);

    if (!env) {
    env = 'dev'; //env can be anything: dev, qa, staging, etc.
    }
    
    var config = {
    		
     baseUrl : 'https://restful-booker.herokuapp.com',
     adminUname : 'admin',
	 adminPwd : 'password123',
     threadCount : 4
    };

    if (env == 'stage') {
        // over-ride only those that need to be
    	config.baseUrl = 'https://restful-booker.herokuapp.com'
        config.adminUname = 'https://restful-booker.herokuapp.com'
        config.adminPwd = 'https://restful-booker.herokuapp.com'
      } else if (env == 'e2e') {
    	  
      }
 // don't waste time waiting for a connection or if servers don't respond within 5 seconds
    karate.configure('connectTimeout', 60000);
    karate.configure('readTimeout', 60000);

    return config;
}